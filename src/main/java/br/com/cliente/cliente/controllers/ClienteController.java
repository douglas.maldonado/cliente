package br.com.cliente.cliente.controllers;

import br.com.cliente.cliente.models.Cliente;
import br.com.cliente.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente createCliente(@RequestBody @Valid Cliente cliente){
        Cliente clienteObjeto = clienteService.create(cliente);

        return clienteObjeto;
    }

    @GetMapping("/{id}")
    public Cliente getCliente(@PathVariable(name = "id") Long id){
        Cliente cliente = clienteService.getClienteId(id);

        return cliente;
    }

}
