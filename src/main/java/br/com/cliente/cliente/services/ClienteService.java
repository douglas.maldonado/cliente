package br.com.cliente.cliente.services;

import br.com.cliente.cliente.exceptions.ClienteNotFoundException;
import br.com.cliente.cliente.models.Cliente;
import br.com.cliente.cliente.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente create(Cliente cliente){
        Cliente clienteObjeto = clienteRepository.save(cliente);

        return clienteObjeto;
    }

    public Cliente getClienteId(Long id){
        Optional<Cliente> optionalCliente = clienteRepository.findById(id);

        if(optionalCliente.isPresent()){
            return optionalCliente.get();
        }

        throw new ClienteNotFoundException();
    }
}
